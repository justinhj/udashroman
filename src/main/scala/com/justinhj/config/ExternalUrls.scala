package com.justinhj.config
object ExternalUrls {
  val bitbucketSource = "https://bitbucket.org/justinhj/udashroman/"

  val homepage = "http://udash.io/"
}
