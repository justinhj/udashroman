package com.justinhj.views

import io.udash._
import com.justinhj._
import org.scalajs.dom.Element
import com.justinhj.styles.{DemoStyles, GlobalStyles}
import scalacss.ScalatagsCss._

object IndexViewPresenter extends DefaultViewPresenterFactory[IndexState.type](() => new IndexView)

class IndexView extends View {
  import com.justinhj.Context._
  import scalatags.JsDom.all._

  private val content = div(
    h2("UDash learning"),
    ul(DemoStyles.stepsList)(
      li(a(DemoStyles.underlineLinkBlack, href := RomanConverterState.url)("Roman numeral converter"))
    )
  )

  override def getTemplate: Modifier = content

  override def renderChild(view: View): Unit = {}
}
