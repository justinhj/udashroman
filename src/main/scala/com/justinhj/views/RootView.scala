package com.justinhj.views

import io.udash._
import com.justinhj.RootState
import org.scalajs.dom.Element
import scalatags.JsDom.tags2.main
import com.justinhj.views.components.{Footer, Header}
import com.justinhj.styles.{DemoStyles, GlobalStyles}
import scalacss.ScalatagsCss._
import io.udash.bootstrap.{BootstrapStyles, UdashBootstrap}
import io.udash.bootstrap.utils.{UdashJumbotron, UdashPageHeader}
import org.scalajs.dom

import scalatags.JsDom.all._

object RootViewPresenter extends DefaultViewPresenterFactory[RootState.type](() => new RootView)

class RootView extends View {
  import com.justinhj.Context._
  import scalatags.JsDom.all._

  private val child: Element = div().render

  private def loadBootstrapStyles(): dom.Element =
    link(rel := "stylesheet", href := "assets/bootstrap/css/bootstrap.min.css").render

  override def getTemplate: Modifier = div(
    loadBootstrapStyles(),
    main(BootstrapStyles.container)(
      div(GlobalStyles.body)(
        div(child))))


  override def renderChild(view: View): Unit = {
    import io.udash.wrappers.jquery._
    jQ(child).children().remove()
    view.getTemplate.applyTo(child)
  }
}
