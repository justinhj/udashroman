package com.justinhj.views

import com.justinhj.RomanConverterState
import com.justinhj.config.ExternalUrls
import com.justinhj.romanconvert.Convert 
import com.justinhj.styles.{DemoStyles, GlobalStyles}
import com.justinhj.styles.utils.StyleUtils
import com.justinhj.views.components._
import io.udash._
import io.udash.bootstrap.form._
import org.scalajs.dom.Element
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Try, Success, Failure}
import scalacss.ScalatagsCss._
import scalacss.internal.Compose
import scalajs.concurrent.JSExecutionContext.Implicits.queue
import scalatags.JsDom.all._

trait ConversionModel {
  def decimal: String
  def roman: String
}

case object RomanConverterViewPresenter extends ViewPresenter[RomanConverterState.type] {

  override def create(): (View, Presenter[RomanConverterState.type]) = {
    val model = ModelProperty[ConversionModel]

    val presenter = new RomanConverterPresenter(model)
    val view = new RomanConverterView(model, presenter)
    (view, presenter)
  }
}

class RomanConverterPresenter(model: ModelProperty[ConversionModel]) extends Presenter[RomanConverterState.type] {

  // Validation function for valid Roman characters
  def validateRoman(roman: String): ValidationResult = {

    if(roman.size == 0) {
      //println("Invalid bad length")
      Invalid("Roman numerals must have at least one Roman character")
    }
    else if(roman.toUpperCase.forall(c => Convert.validRomanChar(c))) {
      //println(s"Valid")
      Valid
    }
    else {
      //println("Invalid bad char")
      Invalid("Roman numerals should contain only valid Roman numeral characters")
    }
  }

  // Validate a roman numeral input
  // Must consist of only the valid Roman numeral characters, case does not matter
  object RomanValidator extends Validator[String] {
    def apply(roman: String)
      (implicit ec: ExecutionContext): Future[ValidationResult] =
      Future {
        validateRoman(roman)
      }
  }

  object DecimalValidator extends Validator[String] {
    def apply(decimal: String)
      (implicit ec: ExecutionContext): Future[ValidationResult] =
      Future {
        //println(s"Validating that $decimal is valid")
        Try(decimal.toInt) match {
          case Success(n) if n > 0 && n <= 100000000 =>
            //println(s"$n is valid")
            Valid
          case Success(n) if n > 0 =>
            //println(s"failed to validate decimal")
            Invalid("Must be a positive number less than 100000000")

          case Success(n) =>
            //println(s"failed to validate decimal")
            Invalid("Must be a positive number")
        }
      }
  }

  // Called before view starts rendering.
  override def handleState(state: RomanConverterState.type): Unit = {

    model.subProp(_.roman).addValidator(RomanValidator)
    model.subProp(_.decimal).addValidator(DecimalValidator)

    model.subProp(_.decimal).listen(d => {

      // If the decimal property is valid convert it to Roman

      model.subProp(_.decimal).isValid.onComplete {
        case Success(Valid) => 

          Try(d.toInt) match {
            case Success(num) =>
              val asRoman = Convert.decimalToRoman(num)

              model.subProp(_.roman).set(asRoman)

            case Failure(err) =>
              println(s"decimal to roman convert error $err")
        }
        case Success(errors) => println(s"$d has validation errors $errors")

        case Failure(err) => println(s"validating $d caused exception $err")
      }


    })

    model.subProp(_.roman).listen{ r =>

      model.subProp(_.roman).isValid.onComplete {
        case Success(Valid) =>
          Convert.safeRomanNumeralsToDecimal(r) match {
            case Right(converted) => model.subProp(_.decimal).set(converted)
            case Left(err) => println(s"$r roman convert error $err")

          }

        case Success(errors) => println(s"$r has validation errors $errors")

        case Failure(err) => println(s"validating $r caused exception $err")

      }
    }

    model.subProp(_.decimal).set("1")
    model.subProp(_.roman).set("I")
  }
}

class RomanConverterView(model: ModelProperty[ConversionModel], presenter: RomanConverterPresenter)
    extends FinalView {

  def convertForm: Modifier = 
    div(
      UdashForm( 
        UdashForm.numberInput
          (validation = Some(UdashForm.validation(model.subProp(_.decimal))))
          ("Decimal")
          (model.subProp(_.decimal)),
        div(DemoStyles.center,
          div(`class` := "glyphicon glyphicon-chevron-up"),br,
          div(`class` := "glyphicon glyphicon-chevron-down")),
        UdashForm.textInput
          (validation = Some(UdashForm.validation(model.subProp(_.roman))))
          ("Roman")
          (model.subProp(_.roman))
      ).render)


  private val content = div(
    h2("Roman Numerals Converter"),
    div(cls := "col-md-6",
      convertForm,
      div(DemoStyles.textVOffset),
      div(`class`:="container",
        "Scala.js source code on ", Image("bitbucket.png", "Bitbucket source", DemoStyles.logo), " ", 
        a(DemoStyles.underlineLinkGrey,
          href:=ExternalUrls.bitbucketSource, ExternalUrls.bitbucketSource)
      ),
      div(`class`:="container",
        "Made with ", Image("udash_logo.png", "Udash Framework", DemoStyles.logo), " ",
        a(DemoStyles.underlineLinkGrey,
          href:=ExternalUrls.homepage, "UDash"),
        " Scala web framework"
      )
    )
  )

  override def getTemplate: Modifier = content

  override def renderChild(view: View): Unit = {}
}
