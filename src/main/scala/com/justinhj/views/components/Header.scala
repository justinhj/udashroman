package com.justinhj.views.components
import com.justinhj.IndexState
import com.justinhj.config.ExternalUrls
import com.justinhj.styles.partials.{HeaderStyles, FooterStyles}
import org.scalajs.dom.raw.Element
import com.justinhj.styles.{DemoStyles, GlobalStyles}
import scalatags.JsDom.all._
import scalacss.ScalatagsCss._
import com.justinhj.Context._

object Header {
  private lazy val template = header(HeaderStyles.header)(
    div(GlobalStyles.body, GlobalStyles.clearfix)(
      div(FooterStyles.footerLinks)(
        ul(
          li(DemoStyles.navItem)(
            a(href := ExternalUrls.bitbucketSource, target := "_blank", DemoStyles.underlineLink)("See the source on bitbucket"))))
    )
  ).render


  def getTemplate: Element = template
}
