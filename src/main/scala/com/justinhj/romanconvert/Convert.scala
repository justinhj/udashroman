package com.justinhj.romanconvert

import scala.collection.immutable.SortedMap

object Convert {

  // Convert from Roman to Decimal

  private val validChars = List[Char]('I' , 'V' , 'X' , 'L' , 'C' , 'D' , 'M')

  private val vm = Map[Char, Int]( ('I', 1), ('V', 5), ('X', 10), ('L', 50), ('C', 100), ('D', 500), ('M', 1000))

  // need to emulate clojure partition with n 2 and step 1
  // filling in default missing values ...
  private def pairUp[A](n: List[A], default: A) : List[(A,A)] = {

    @scala.annotation.tailrec
    def helper(n: List[A], acc: List[(A,A)]) : List[(A,A)] = {

      n match {
        case a :: b :: rest =>
          helper(b :: rest, (a,b) :: acc)

        case a :: Nil =>
          (a, default) :: acc

        case Nil =>
          acc
      }
    }

    helper(n, List.empty[(A,A)]).reverse
  }

  // This assumes only valid Roman numerals are passed in
  // must validate a string that only contains valid letters
  def romanNumeralsToDecimal(input : String): Int = {

    val strVals = input.map{vm.getOrElse(_, 0)}.toList

    val paired = pairUp(strVals, 0)

    paired.foldLeft(0){
      case (acc, (a,b)) => {
        if (a >= b) acc + a
        else acc + (b - a) - b
      }
    }
  }

  def safeRomanNumeralsToDecimal(input: String): Either[String, String] = {

    val upcase = input.toUpperCase()

    if(!upcase.forall(vm.contains(_)))
      Left(s"Invalid characters in input: $input")
    else {
     Right(romanNumeralsToDecimal(upcase).toString)
    }
  }

  // Convert from decimal to Roman

  val svm = SortedMap(
    (1000,"M"),
    (500,"D"),
    (350,"LC"),
    (100,"C"),
    (50,"L"),
    (40,"XL"),
    (10,"X"),
    (5,"V"),
    (4,"IV"),
    (1,"I"),
    (9,"IX"),
    (900,"CM"),
    (90,"XC"))(Ordering[Int].reverse)

  def decimalToRoman(r: Int, o: String = "") : String = {

    if(r == 0) {
      if(o.isEmpty) "0" else o
    }
    else {
      svm.find{ case (value, _) => value <= r } match {
        case Some((v,s)) => decimalToRoman(r - v, o + s)
        case None => o
      }

    }

  }

  def validRomanChar(c : Char) : Boolean = validChars.contains(c)

}
