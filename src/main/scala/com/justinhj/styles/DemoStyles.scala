package com.justinhj.styles

import java.util.concurrent.TimeUnit

import com.justinhj.styles.utils.{MediaQueries, StyleUtils}
import io.udash.bootstrap.{BootstrapStyles, UdashBootstrap}
import scala.concurrent.duration.FiniteDuration
import scala.language.postfixOps
import scalacss.internal.Compose
import scalacss.DevDefaults._

object DemoStyles extends StyleSheet.Inline {
  import dsl._

  val linkHoverAnimation = keyframes(
    (0 %%) -> keyframe(color.black),
    (50 %%) -> keyframe(color.red),
    (100 %%) -> keyframe(color.black)
  )

  val center = style (
    textAlign.center
  )

  val navItem = style(
    position.relative,
    display.inlineBlock,
    verticalAlign.middle,
    paddingLeft(1.8 rem),
    paddingRight(1.8 rem),

    &.firstChild (
      paddingLeft(0 px)
    ),

    &.lastChild (
      paddingRight(0 px)
    ),

    &.before.not(_.firstChild)(
      StyleUtils.absoluteMiddle,
      content := "\"|\"",
      left(`0`),

      &.hover(
        textDecoration := "none"
      )
    )
  )

  val underlineLink = style(
    position.relative,
    display.block,
    color.darkgrey,

    &.after(
      StyleUtils.transition(transform, new FiniteDuration(250, TimeUnit.MILLISECONDS)),
      position.absolute,
      top(100 %%),
      left(`0`),
      content := "\" \"",
      width(100 %%),
      borderBottomColor.darkgrey,
      borderBottomWidth(1 px),
      borderBottomStyle.solid,
      transform := "scaleX(0)",
      transformOrigin := "100% 50%"
    ),

    &.hover(
      color.darkgrey,
      cursor.pointer,
      textDecoration := "none",

      &.after (
        transformOrigin := "0 50%",
        transform := "scaleX(1)"
      )
    )
  )

  val underlineLinkGrey = style(
    underlineLink,
    display.inlineBlock,
    color.darkgrey,

    &.visited(
      color.lightgrey
    ),

    &.after(
      borderBottomColor.darkgrey
    ),

    &.hover (
      color.grey
    )
  )(Compose.trust)

  val underlineLinkBlack = style(
    underlineLink,
    display.inlineBlock,
    color.darkgrey,

    &.after(
      borderBottomColor.black
    ),

    &.hover (
      color.black
    )
  )(Compose.trust)

  private val liBulletStyle = style(
    position.absolute,
    left(`0`),
    top(`0`)
  )

  val textVOffset = style( marginTop(30 px) )

  val inputLabelUnder = style(
    position.relative,
    top(35 px),
    left(50 px)
  )

  private val liStyle = style(
    position.relative,
    paddingLeft(2 rem),
    margin(.5 rem, `0`, .5 rem, 4.5 rem),

    MediaQueries.phone(
      style(
        marginLeft(1.5 rem)
      )
    )
  )

  val stepsList = style(
    counterReset := "steps",
    unsafeChild("li") (
      liStyle,

      &.before(
        liBulletStyle,
        counterIncrement := "steps",
        content := "counters(steps, '.')\".\""
      )
    )
  )

  val logoInner = style(
    StyleUtils.relativeMiddle,

    MediaQueries.phone(
      style(
        top.auto,
        transform := "none"
      )
    )
  )

  val logo = style(
    display.inlineBlock,
    verticalAlign.middle,
    height(32 px),
//    marginRight(25 px)
  )

}
