# Udash Roman numeral converter example

## Summary

In order to explore the Scala web application framework [Udash](http://udash.io/) and experiment with porting Clojure code to Scala code in general I decided to rewrite this [Roman to Decimal converter](https://github.com/justinhj/cljs-roman) I wrote in Clojurescript a few years ago to Scala.

## Implementation

This project uses the Udash generator to make a sample project then I deleted all the stuff I didn't want and implemented a single view that provides the conversion.

You'll find the actual code in `com.justinhj.romanconvert.Convert` where there are two public functions for converting back and forth.

## Usage

* sbt fastOptJS
* Browse to [http://localhost:12345/target/UdashStatic/WebContent/index.html#/roman](http://localhost:12345/target/UdashStatic/WebContent/index.html#/roman)
* Or browse from the generated static files at ~/udashroman/target/UdashStatic/WebContent/index.html






